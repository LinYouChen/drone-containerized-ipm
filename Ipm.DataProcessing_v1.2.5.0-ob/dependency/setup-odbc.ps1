# ODBC setting form env.ps1 
#--------------------------------

echo "IPM ODBC Setting 2020/03/16"
echo "IPM Database: $IPM_DB_NAME"
echo "IP: $IPM_DB_IP"
echo "IPM Database: $IPM_STDB_NAME"
echo "IP: $IPM_STDB_IP"
echo "----"
Add-OdbcDsn -Name "${IPM_DB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "User" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_DB_IP}")
Add-OdbcDsn -Name "${IPM_STDB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "User" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_STDB_IP}")

Add-OdbcDsn -Name "${IPM_DB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "System" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_DB_IP}")
Add-OdbcDsn -Name "${IPM_STDB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "System" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_STDB_IP}")

$global:IPM_DB_NAME="IPM_LITE_5_DB_CHAD"
$global:IPM_DB_IP="140.116.86.156"
$global:IPM_DB_USER="IPM_LITE_5_DB_CHAD"
$global:IPM_DB_PASSWD="IPM_LITE_5_DB_CHAD"

#IPM STDB information
$global:IPM_STDB_NAME="IPM_LITE_5_STDB_CHAD"
$global:IPM_STDB_IP="140.116.86.156"
$global:IPM_STDB_USER="IPM_LITE_5_STDB_CHAD"
$global:IPM_STDB_PASSWD="IPM_LITE_5_STDB_CHAD"
#--------------------------------
echo "IPM Database: $IPM_DB_NAME"
echo "IP: $IPM_DB_IP"
echo "IPM Database: $IPM_STDB_NAME"
echo "IP: $IPM_STDB_IP"
echo "----"

Add-OdbcDsn -Name "${IPM_DB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "User" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_DB_IP}")
Add-OdbcDsn -Name "${IPM_STDB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "User" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_STDB_IP}")

Add-OdbcDsn -Name "${IPM_DB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "System" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_DB_IP}")
Add-OdbcDsn -Name "${IPM_STDB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "System" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_STDB_IP}")

# ODBC setting for 192.158.0.x
#--------------------------------
$IPM_DB_IP="192.168.0.144"
$IPM_STDB_IP="192.168.0.144"

#Master Database
$IPM_DB_NAME='IPM_DB_MASTER_0144'
$IPM_STDB_NAME='IPM_STDB_MASTER_0144'

echo "IPM Database: $IPM_DB_NAME"
echo "IP: $IPM_DB_IP"
echo "IPM Database: $IPM_STDB_NAME"
echo "IP: $IPM_STDB_IP"
echo "----"

Add-OdbcDsn -Name "${IPM_DB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "User" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_DB_IP}")
Add-OdbcDsn -Name "${IPM_STDB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "User" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_STDB_IP}")

Add-OdbcDsn -Name "${IPM_DB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "System" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_DB_IP}")
Add-OdbcDsn -Name "${IPM_STDB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "System" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_STDB_IP}")

#Slave Database
$IPM_DB_NAME='IPM_DB_SLAVE_0144'
$IPM_STDB_NAME='IPM_STDB_SLAVE_0144'

echo "IPM Database: $IPM_DB_NAME"
echo "IP: $IPM_DB_IP"
echo "IPM Database: $IPM_STDB_NAME"
echo "IP: $IPM_STDB_IP"
echo "----"

Add-OdbcDsn -Name "${IPM_DB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "User" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_DB_IP}")
Add-OdbcDsn -Name "${IPM_STDB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "User" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_STDB_IP}")

Add-OdbcDsn -Name "${IPM_DB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "System" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_DB_IP}")
Add-OdbcDsn -Name "${IPM_STDB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "System" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_STDB_IP}")



# ODBC setting for 192.158.50.x
#--------------------------------
$IPM_DB_IP="192.168.50.100"
$IPM_STDB_IP="192.168.50.100"

#Master Database
$IPM_DB_NAME='IPM_DB_MASTER_50100'
$IPM_STDB_NAME='IPM_STDB_MASTER_50100'

echo "IPM Database: $IPM_DB_NAME"
echo "IP: $IPM_DB_IP"
echo "IPM Database: $IPM_STDB_NAME"
echo "IP: $IPM_STDB_IP"
echo "----"

Add-OdbcDsn -Name "${IPM_DB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "User" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_DB_IP}")
Add-OdbcDsn -Name "${IPM_STDB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "User" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_STDB_IP}")

Add-OdbcDsn -Name "${IPM_DB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "System" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_DB_IP}")
Add-OdbcDsn -Name "${IPM_STDB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "System" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_STDB_IP}")

#Slave Database
$IPM_DB_NAME='IPM_DB_SLAVE_50100'
$IPM_STDB_NAME='IPM_STDB_SLAVE_50100'

echo "IPM Database: $IPM_DB_NAME"
echo "IP: $IPM_DB_IP"
echo "IPM Database: $IPM_STDB_NAME"
echo "IP: $IPM_STDB_IP"
echo "----"

Add-OdbcDsn -Name "${IPM_DB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "User" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_DB_IP}")
Add-OdbcDsn -Name "${IPM_STDB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "User" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_STDB_IP}")

Add-OdbcDsn -Name "${IPM_DB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "System" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_DB_IP}")
Add-OdbcDsn -Name "${IPM_STDB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "System" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_STDB_IP}")


# ODBC setting for CDB
#---------------
$IPM_DB_IP="10.110.20.1"
$IPM_STDB_IP="10.110.20.1"

for ($i = 1; $i -le 32; $i ++){
	$IPM_DB_NAME="IPM_DB_"+$i
	$IPM_STDB_NAME="IPM_STDB_"+$i


	echo "IPM Database: $IPM_DB_NAME"
	echo "IP: $IPM_DB_IP"
	echo "IPM Database: $IPM_STDB_NAME"
	echo "IP: $IPM_STDB_IP"
	echo "----"

	Add-OdbcDsn -Name "${IPM_DB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "User" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_DB_IP}")
	Add-OdbcDsn -Name "${IPM_STDB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "User" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_STDB_IP}")

	Add-OdbcDsn -Name "${IPM_DB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "System" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_DB_IP}")
	Add-OdbcDsn -Name "${IPM_STDB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "System" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_STDB_IP}")
}