$PATH='C:\app\App_Data'

# Create config.xml

Write-Output '<?xml version="1.0" encoding="utf-8"?>' > .\config.xml
Write-Output '' >> .\config.xml
Write-Output '<Settings>' >> .\config.xml

Set-Location $PATH
if($IPM_APP_TYPE -eq "DataProcessing") {
  #Data Processing
  Write-Output "  <Name>$IPM_APP_ROLE</Name>" >> .\config.xml
  Write-Output '  <DB>' >> .\config.xml
  Write-Output "    <DSN>$IPM_DB_NAME</DSN>" >> .\config.xml
  Write-Output "    <UID>$IPM_DB_UID</UID>" >> .\config.xml
  Write-Output "    <PWD>$IPM_DB_PWD</PWD>" >> .\config.xml
  Write-Output '  </DB>' >> .\config.xml
  Write-Output '  <STDB>' >> .\config.xml
  Write-Output "    <DSN>$IPM_STDB_NAME</DSN>" >> .\config.xml
  Write-Output "    <UID>$IPM_STDB_UID</UID>" >> .\config.xml
  Write-Output "    <PWD>$IPM_STDB_PWD</PWD>" >> .\config.xml
  Write-Output '  </STDB>' >> .\config.xml
  Write-Output '' >> .\config.xml
  if($IPM_APP_ROLE -eq "Slave") {
    #Data Processing Slave
    Write-Output "  <UrlOfAlgService>$URL_OF_IPM_ALG_SERVICE</UrlOfAlgService>" >> .\config.xml
    Write-Output "  <UrlOfMaster>$IPM_DATA_PROCESSING_SERVER</UrlOfMaster>" >> .\config.xml
    Write-Output '</Settings>' >> .\config.xml
  }else{
    #Data Processing Master
    Write-Output '  <UrlOfSlaves>' >> .\config.xml
    foreach($slave in $IPM_DATA_PROCESSING_CLIENT){
      Write-Output "    <Url>$slave</Url>">> .\config.xml
    }
    Write-Output '  </UrlOfSlaves>' >> .\config.xml  
  }
  Write-Output '</Settings>' >> .\config.xml

  #Setup ODBC
  Add-OdbcDsn -Name "${IPM_DB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "User" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_DB_IP}")
  Add-OdbcDsn -Name "${IPM_STDB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "User" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_STDB_IP}")
	cd C:\app

  #Run the App
  dotnet.exe "Ipm.DataProcessing.dll"

}

#GUI
if($IPM_APP_TYPE -eq "GUI") {
  #Data Processing
  Write-Output "  <UrlOfDataProcessing>$IPM_DATA_PROCESSING</UrlOfDataProcessing>" >> .\config.xml
  dotnet.exe "Ipm.DataProcessing.dll"
  Write-Output '</Settings>' >> .\config.xml

  #Run the App
  dotnet.exe "Ipm.Gui.dll
  
}

