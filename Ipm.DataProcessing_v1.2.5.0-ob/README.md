# Containerized IPM

This project can build the containerized IPM and generate IPM docker image.
### TO-Do Using Submodule to include IPM Source Code

### Prerequisites

* Windows Server 2019 Datacenter (version 1809, OS BUILD:17763.107)
* Docker 19.03.5

### Installing

#### Step 1: 

Put all application projects in .\source folder, the folder structure may like this:
```
#cd .\source
#tree -L 1
.
├── IPM
├── IPM.Algo
├── IPM.BasicComponents_
└── IPM.Rul
```
(You can skip this step, if you don't want to update application version.)

#### Step 2: 

Put the Config.xml in the .\dependency subfolder. (You can find the example in .\dependency\Config-example.xml)

#### Step 3: 

Create .\env.ps1 file to set the environment parameters, you can rename .\env-example.ps1 to .\env.ps1 and edit it:
```
Rename-Item -Path .\env-example.ps1 -NewName .\env.ps1
```

#### Step 4: 

Run the following command:
```
.\build.ps1
```

## Authors

* **James Shieh** - *Initial work* - [PurpleBooth](https://gitlab.com/jamesshieh/containerized-ipm)

