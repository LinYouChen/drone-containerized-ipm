# Script For Debug

# copy files from a container to host
$container_list=docker ps
$container_id=$container_list[1].split()[0]
$container_path='C:/app/App_Data/'
$host_path='C:/'
docker cp ${container_id}:${container_path} $host_path
open $host_path

# enter the container using cmd.exe
$container_list=docker ps
$container_id=$container_list[1].split()[0]
docker exec -it ${container_id} cmd.exe

# build a test image
$image_name='ipm-test'
$image_version='latest'
docker build -t ${image_name}:${image_version} -f Dockerfile-test .
docker run -p 8000:80 -it ${image_name}:${image_version} cmd.exe

# delete all stoped images
docker rm $(docker ps -a -q)

# remove all none tag images
docker rmi $(docker images -f "dangling=true" -q)

#run the ipm container
# build a test image
$image_name='ipm'
$image_version='v1.0.0-build-v1.1.3.0'
docker run -p 8000:80 -it ${image_name}:${image_version} cmd.exe
